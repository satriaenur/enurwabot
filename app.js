var sulla = require('sulla');
var express = require('express');
var cors = require('cors')
var app = express();

app.use(express.json());
app.use(cors())

sulla.create().then(function(client){
	app.post('/sendMessage', function (req, res) {
		var body = req.body;
		client.sendMessageToID(`${body.to}@c.us`, `${body.message}`);
		res.json(body.to);
	});

	app.listen(3000, function () {
        	console.log('Example app listeing on port 3000!');
	});
});

